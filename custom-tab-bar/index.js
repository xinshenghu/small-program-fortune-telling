Component({
  data: {
    selected: 0,
    color: "#7A7E83",
    selectedColor: "#3cc51f",
    list: [{
        "pagePath": "/pages/index/index",
        "iconPath": "/img/1-n.png",
        "selectedIconPath": "/img/1-y.png",
        "text": "首页"
      }, {
        "pagePath": "/pages/xingzuo/index",
        "iconPath": "/img/2-n.png",
        "selectedIconPath": "/img/2-y.png",
        "text": "星座"
      }, {
        "pagePath": "/pages/xuexing/index",
        "iconPath": "/img/3-n.png",
        "selectedIconPath": "/img/3-y.png",
        "text": "血型"
      },
      {
        "pagePath": "/pages/mianxiang/index",
        "iconPath": "/img/4-n.png",
        "selectedIconPath": "/img/4-y.png",
        "text": "面相"
      },
      {
        "pagePath": "/pages/shuxiang/index",
        "iconPath": "/img/5-n.png",
        "selectedIconPath": "/img/5-y.png",
        "text": "属相"
      }
    ]
  },
  attached() {},
  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset
      const url = data.path
      wx.switchTab({
        url
      })
      this.setData({
        selected: data.index
      })
    }
  }
})