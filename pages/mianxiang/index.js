// pages/xingzuo/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: [{
        key: 1,
        message: '眼相',
      },
      {
        key: 2,
        message: '鼻相'
      },
      {
        key: 3,
        message: '嘴相'
      },
      {
        key: 4,
        message: '耳相'
      },
      {
        key: 5,
        message: '眉相'
      }, {
        key: 6,
        message: '脸相'
      }
    ]
  },
  go: function (e) {
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../mianxiang1/index?id=' + id
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (typeof this.getTabBar == "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 3
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})