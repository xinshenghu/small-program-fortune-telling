Page({

  /**
   * 页面的初始数据
   */
  data: {
    shuoming: '',
    xianshi: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options.id)
    let _this = this
    // console.log(Math.ceil(Math.random()*3));
    switch (options.id) {
      case '1':
        this.setData({
          shuoming: "A型——崇尚完美主义者"
        })

        this.setData({
          xianshi: "他们特别重视别人对自己的看法与评价，对建立和谐的人际关系具有强烈的渴望。为了免于得罪别人，A型人处事谨慎，待人诚恳。社会生活中的A型人深沉含蓄，感情不外露，但内心却潜藏着巨大的爆发力，因为他们总是压抑自己，自制力越强，爆发起来也就越可怕。          　　待人接物细心谨慎，避免伤害他人的感情，也不给对方或周围的人带来不愉快。有克制自己，积极为人服务的一面和不肯轻易相信人--说得坏一点 就是多疑的特点。我们可以把这看作是A型人的两重性之一。"
        })
        break;
      case '2':
        this.setData({
          shuoming: "B型——充满感情的行动家"
        })

        _this.setData({
          xianshi: "B型人是自由主义者B型人和A型人恰好相反，他们很少顾虑社会的反映与舆论的压力，喜欢独立的处理问题。B型人言行随便、不拘小节，反感限制与拘束，甚至发展到脱离集体。          　　B型人处理问题的方式比较灵活，这点也和A型的人相反，他们喜欢用科学的观点处理问题，就事论事，并且很少草率的下结论，所得的结论也具有很大的变通的余地。"
        })
        break;
      case '3':
        this.setData({
          shuoming: "O型——现实浪漫主义者"
        })

        _this.setData({
          xianshi: "O型人是现实主义者O型人富于现实感，能够迅速、冷静、准确对利害得失进行分析和判断；他们的目的性比较强，确定目标后，会果断的采取措施，并且不达目的不罢休。          　　O型的人是现实主义者，关心自己的健康，热爱生命，但也不乏浪漫，喜欢运用优美的语言，欣赏有目的的生活，并在不同程度还是走向理想主义。O型的人渴望获得力量，注重集体的力量，重视人际关系。"
        })
        break;
      case '4':
        this.setData({
          shuoming: "AB型——充满矛盾的自信家"
        })

        _this.setData({
          xianshi: "AB型是空想主义者。AB型的人具有强烈的社会意识和集体观念，他们有意识的融入社会活动，希望在社会生活中确定自己的位置，满足其强烈的归属感。AB型的人生性淡泊。          　　他们的逻辑思维比较见长，擅长分析和批评，但是由于他们喜欢站在第三者的立场，所以总是给人留下比较冷漠的印象。AB型的考虑问题比较全面，解决问题的速度也比较快，但是思考问题的深度不足；AB型常常抱有超越现实的空想。"
        })
        break;

      default:
        break;
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})