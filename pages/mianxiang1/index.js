Page({

  /**
   * 页面的初始数据
   */
  data: {
    shuoming: '',
    xianshi: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options.id)
    let _this = this
    // console.log(Math.ceil(Math.random()*3));
    switch (options.id) {
      case '1':
        this.setData({
          shuoming: "龙眼 杏眼 凤眼 桃花眼"
        })


        let obj1 = {}
        obj1.text = "龙眼 眼睛圆的人个性坦率，乐观而天真，善于表现自我，社交能力极佳，属于自来熟的类型。喜欢浪漫的事物，但占有欲强，且带有一些投机的心态。"
        obj1.img = '/img/mianxiang/longyan.jpg'

        let obj2 = {}
        obj2.text = " 杏眼 眼睛大的人为人大多爽朗热诚，有时也会是爱表现出风头的人。当然也附带了大大咧咧的迷糊特性。感受力很强，是有些天然感的类型。喜欢华美的事物，因此也有虚荣心态。"
        obj2.img = '/img/mianxiang/xingyan.jpg'

        let obj3 = {}
        obj3.text = " 凤眼  眼睛又细又长其人较为傲气，这样的人心态比较高傲处事还很有可能自带清高。凡事喜欢速战速决，富有决断力，但为人处事镇定稳妥也能通过深思熟虑之后再付诸行动。"
        obj3.img = '/img/mianxiang/fengyan.jpg'

        let obj4 = {}
        obj4.text = " 桃花眼 极富魅力 有“眼带桃花”眼相的人往往非常迷人,魅力十足,异性缘极好,即使年龄逐渐变大,桃花运也会只增不减。这种眼型的人看人的时候下眼睑向上弯,似笑非笑,似有泪光浮现,眼含深情,让人沉醉其中。 "
        obj4.img = '/img/mianxiang/taohuayan.jpg'

        this.setData({
          xianshi: [obj1, obj2, obj3, obj4]
        })
        break;
      case '2':
        this.setData({
          shuoming: "低鼻子 高鼻子 长鼻子 短鼻子"
        })

        let dibizi = {}
        dibizi.text = "低鼻子——没有主见 这种人没有主见，做事也欠考虑，一般听从他人的意见行事，自己没有信念和方针，经常犹豫不决，所以在人生道路上不平稳。"
        dibizi.img = '/img/mianxiang/dibizi.jpg'

        let gaobizi = {}
        gaobizi.text = " 高鼻子——自尊心强 这种人自尊心很强，喜欢按自己的机会行事，能力强，做任何事都能应付自如。人际关系虽然不错，但不会与自己讨厌的人相处。他们很注重外表的修饰。"
        gaobizi.img = '/img/mianxiang/gaobizi.jpg'

        let changbizi = {}
        changbizi.text = " 长鼻子——慎重 长鼻子的人责任感强，做事认真，但过于注重体面，处事也过于慎重，有自我束缚的倾向，给人一种骄傲自大的印象，并且不善于理财。"
        changbizi.img = '/img/mianxiang/changbizi.jpg'

        let duanbizi = {}
        duanbizi.text = " 短鼻子——处事草率　这种人性情秉直、单纯，给人一种轻松、舒适的感觉。他们并不是靠信念来完成工作的类型。鼻头发达的人-鼻子发达的人性格开朗，做事草率，但富于社交能力，为人大方、坦诚，又有胆识，因此身边总是聚集一些朋友。鼻头不发达的人-鼻子不发达的人没有凝聚力和计划性，并且欠缺责任感，经常犯错误。八字算命 "
        duanbizi.img = '/img/mianxiang/duanbizi.jpg'

        _this.setData({
          xianshi: [dibizi, gaobizi, changbizi, duanbizi]
        })
        break;
      case '3':
        this.setData({
          shuoming: "大嘴 小嘴 仰月口 覆船口"
        })

        let dazui = {}
        dazui.text = "大嘴   大嘴的人热情，大方，好交际，爱应酬，所以朋友多，人际关系好，到哪里都能吃得开，在工作上也比较如鱼得水。但是女人嘴大并不是太好，老话讲：女人嘴大吃死郎。意思就是说女人嘴太大会给老公破财。"
        dazui.img = '/img/mianxiang/dazui.jpg'

        let xiaozui = {}
        xiaozui.text = " 小嘴   嘴小的人传统保守，感情细腻，顾家，不是太爱交际，所以女人嘴小好一点，为贤妻，但是男人嘴小不太好，因为男人嘴小代表度量小，没有容人之量，不符合男子汉的气质。"
        xiaozui.img = '/img/mianxiang/xiaozui.jpg'

        let yangyuekou = {}
        yangyuekou.text = " 仰月嘴：嘴巴弯弯似仰月  仰月口嘴甜心善，语言表达能力很好，很讨人喜欢，运气也不错，主要是财运很好，所以说嘴甜的人招财，也是有道理的。还有这种人头脑很聪明，很有学问，为人比较义气，并且这种人生财有道，也是理财高手。如果是女人长了这种口型还会旺夫，夫荣妻贵，很是令人羡慕。"
        yangyuekou.img = '/img/mianxiang/yangyuezui.jpg'

        let fuchuankou = {}
        fuchuankou.text = " 覆船嘴：嘴角下垂   覆船口的人分两种情况。第一种情况是覆船口加尖下巴，这种人贪心，固执，不好与人相处，运势也不佳，会贫苦，特别是中晚年后生活比较艰辛。第二种情况是覆船口加丰满的下巴，虽然这种人性格上也有上述的缺点，但是运势好很多，因为下巴丰满的话晚年的运势还是很不错的，所以这种类型是比较容易有钱的。 "
        fuchuankou.img = '/img/mianxiang/fuchuanzui.jpg'

        _this.setData({
          xianshi: [dazui, xiaozui, yangyuekou, fuchuankou]
        })
        break;
      case '4':
        this.setData({
          shuoming: "厚耳垂 无耳垂 厚耳朵 薄耳朵"
        })
        let houerchui = {}
        houerchui.text = "厚耳垂   厚耳垂和大耳朵一样，都是命中富贵的有福之人。厚耳垂的人，身体硬朗，金钱运极佳，为人友善，人际关系较好，所以人生比较顺利，家庭幸福。如果大家身边有耳垂肥厚甚至放下米粒的人，那么此人更是显富显贵之人。"
        houerchui.img = '/img/mianxiang/houerchui.jpg'

        let wuerchui = {}
        wuerchui.text = " 无耳垂   没耳垂的人想象力丰富，爱幻想，做事情缺少计划，有点不切实际，但是颇有人情味，是个性情中人。如果是锐角三角形般的耳垂，这种耳相往往和金钱没有缘分。如果整个耳朵长得小而薄又无耳垂的人，这也是不聚财的耳相，这种破财很有可能是在浪费或者失败下造成。所以，没耳垂的人赚钱不是一件易事，这类面相的人应该多多培养自己的理财观念，提高理财能力才能聚财。"
        wuerchui.img = '/img/mianxiang/wuerchui.jpg'

        let houerduo = {}
        houerduo.text = " 厚耳朵          耳朵有肉且肥厚的人，不仅能招财，更是长寿的象征。如果是耳朵又圆又厚，轮廓也较为清晰的人，极有可能成为造财的富豪。拥有这种耳相的人，做事有魄力，善于任用人才来经营自己的事业，思维敏捷，善于应对各种突发状况，是个做大事的人。"
        houerduo.img = '/img/mianxiang/houerduo.jpg'

        let baoerduo = {}
        baoerduo.text = " 薄耳朵          耳朵肉薄，是不聚财的耳相。这种人缺乏自信，会因小事耿耿于怀，容易有神经过敏的倾向，所以导致失眠、食欲不振等病症。这种耳相的人，缺乏赚钱的能力，往往没有太多积蓄。如果一个人的耳朵轮廓清晰漂亮，但是耳朵肉薄，那么这种人通常赚钱容易花钱也容易，没法存钱。这类人可能会在文化艺术上有很高的造诣，但是较难守财。 "
        baoerduo.img = '/img/mianxiang/baoerduo.jpg' 

        _this.setData({
          xianshi: [houerchui, wuerchui, houerduo, baoerduo]
        })
        break;
      case '5':
        this.setData({
          shuoming: "清秀眉 短促眉 柳叶眉 一字眉"
        })

        let qingxiumei = {}
        qingxiumei.text = "清秀眉 清秀眉是一种极为常见的眉形，这种眉毛的特点是眉型虽然长，但不会遮挡住印堂，眉毛略微弯曲，长度过眼睛，眉身不是特别宽。从整体来看，眉毛不浓也不稀。长了这种眉毛的人，为人比较聪明能干，品行也十分端正。这样的人年轻的时候就会有发达的机遇，如果从事官宦一途，也会有很不错的成就。"
        qingxiumei.img = '/img/mianxiang/qingxiumei.jpg'

        let duancumei = {}
        duancumei.text = " 短促秀眉  有这种眉毛的人，印堂特别宽，并且不会被眉毛遮挡。这种眉形的眉身虽然短，但是眉尾能够长过眼睛。 有这种眉毛的人，性格十分仁义忠孝，一生的运气也很好，能有不凡的成就，这样的人寿命特别长，晚年也有不错的富贵。"
        duancumei.img = '/img/mianxiang/duancumei.jpg'

        let liuyemei = {}
        liuyemei.text = " 柳叶眉　顾名思义，柳叶眉的眉毛形状呈现出和柳叶一样的弯曲，柳叶眉的眉身很宽，眉毛顺生并且眉尾整齐有型。　长有柳叶眉的人，多为人忠厚讲信用，这样的人对朋友真诚守信，一生中能有发达扬名的机会。不少性格忠义的人，都长有这样的眉毛。"
        liuyemei.img = '/img/mianxiang/liuyemei.jpg'

        let yizimei = {}
        yizimei.text = " 一字眉 　一字眉的形状和汉字中的“一”字相似，眉头和眉尾齐平，整体眉形清秀，并且眉毛不会遮挡住印堂。有一字眉的人，和清秀眉一样，年轻的时候就能够发达，这样的人性格直爽果断，值得依靠。长有一字眉的人无论从事文职还是武职，都能有不错的前程。在家庭方面，也能够和另一半白头偕老。 "
        yizimei.img = '/img/mianxiang/yizimei.jpg'

        _this.setData({
          xianshi: [qingxiumei, duancumei, liuyemei, yizimei]
        })
        break;
      case '6':
        this.setData({
          shuoming: "国字脸 瓜子脸 鹅蛋脸 圆脸"
        })

        let guozilian = {}
        guozilian.text = "国字脸属于上进、负责任的类型。这类型的男人给人一种正直、可信的感觉，不仅事业有成，而且一生的运气不断，一生都是不愁吃穿的人，而且这类人的性格大多都是比较随和的，这类男人结婚之后，对老婆和孩子非常负责，会变得很努力，事业和财运上会因为努力而取得成就。"
        guozilian.img = '/img/mianxiang/guozilian.jpg'

        let guazilian = {}
        guazilian.text = " 瓜子脸脸型是东方女性崇尚的脸型，其优点是“非常抢镜”。因为瓜子脸脸型显得脸小，被镜头放大了比较好看。综观瓜子脸的人，多数都是照片比本人好看的。            瓜子脸脸型的人多是完美主义者，有的有轻微洁癖，严以律己、宽以待人，社交手腕老练。"
        guazilian.img = '/img/mianxiang/guazilian.jpg'

        let edanlian = {}
        edanlian.text = " 鹅蛋脸的人比较注重生活质量和享受,所以开支也是比较大的,尤其突然增加的开支可是不少呀。在日常的理财和投资上,鹅蛋脸的人应该多反省和控制调整一下。尤其是拥有鹅蛋脸的女性们,在日常购物开销方面是比较大的,大花绝对是财运的弊端哦。"
        edanlian.img = '/img/mianxiang/edanlian.jpg'

        let yuanlian = {}
        yuanlian.text = "圆脸型的人，其性格如同亮相，体格比较圆润，为人很好，心地善良，具有菩萨心肠，从不做害人的事情，心胸宽广，圆脸型的人是现实派，不与说过不去，也不会有警惕之心，也属于乐天派，知足常乐，随遇而安，不贪婪，是一种福相脸型，是做生意的一把好手，有钱赚也有福享，更具有旺夫运。"
        yuanlian.img = '/img/mianxiang/yuanlian.jpg'

        _this.setData({
          xianshi: [guozilian, guazilian, edanlian, yuanlian]
        })
        break;

      default:
        break;
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})