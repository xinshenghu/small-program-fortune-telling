Page({

  /**
   * 页面的初始数据
   */
  data: {
    shuoming: '',
    xianshi: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options.id)
    let _this = this
    // console.log(Math.ceil(Math.random()*3));
    switch (options.id) {
      case '1':
        this.setData({
          shuoming: "性格优点  1、重视感情、有很大的志向、善理财、聪明、 精力充沛、一丝不苟、善于社交、幽默。  2、机智、点子多，善解人意。 3、受人欢迎、有吸引力。  "
        })

        this.setData({
          xianshi: "性格缺点 1、做事魄力不够。 2、有固执已见之性格，有见利妄行之缺点。 3、有晚睡习惯。 "
        })
        break;
      case '2':
        this.setData({
          shuoming: "性格优点  1、做事谨慎小心，脚踏实地行动缓慢、体格强壮、毅力强、自我牺牲。 2、依照自已意念和能力做事。  3、在采取行动之前，早有一番深思熟虑，而且有始有终拥有坚强的信念和强壮的体力。"
        })

        _this.setData({
          xianshi: "性格缺点  1、女性较缺乏娇柔，如果能意认到自己的不足，改变一下拘谨冷漠的态度表现自已则在感情上亦能称心如意。      2、因任劳任怨加上个性固执不听劝告，时常忘了进餐，而有肠胃问题。   3、如顽石般不知变通且毫无情趣。 "
        })
        break;
      case '3':
        this.setData({
          shuoming: "性格优点  1、个性较为固执强硬专断独行，喜冒险逞强，越挫越猛，雄心万丈，对自己充满信心。 2、富男子气慨且热情勇敢，冒险精神过于常人。 3、做事积极大胆表达自己，处事有霸道。  "
        })

        _this.setData({
          xianshi: "性格缺点  1、颇具叛逆性往往过于自信无法与他人协调变通，喜欢独来独往，经常表现极瑞性。  2、缺乏浪漫情调，对待妻子也使用独裁手腕，缺乏愉快的家庭生活。 3、相识虽广但都无法深交，固执己见，为达目的不择手段，专橫霸道，喜欢剌激，自我意识强。 "
        })
        break;
      case '4':
        this.setData({
          shuoming: "性格优点  1、心思细密，个性温柔体贴体人。 2、有语言天才与犀利的口才，颇受人欢迎。  3、个性善变，相当保守，头脑冷静。  "
        })

        _this.setData({
          xianshi: "性格缺点  1、有博爱及众的傾向及大众情人时心态，易生感情糾紛。  2、缺乏思虑决断力，常因多情失败。 3、表面好好先生且凡事唯唯是诺，然而內心卻相当顽固。"
        })
        break;
      case '5':
        this.setData({
          shuoming: "性格优点  1、有强壮的体魄，精力充沛活力， 朝气勃勃，有高尚的理想，富罗曼蒂克气氛，是爱面子派头的人。 "
        })

        this.setData({
          xianshi: "性格缺点 1、情绪不稳，富梦想，茫然不可捉摸，性格傲慢缺乏宽大心胸。 "
        })
        break;
      case '6':
        this.setData({
          shuoming: "性格优点  1、有神秘浪漫斯文外表与熟练处世态度，风度翩翩善于辞令很会钻营。 "
        })

        _this.setData({
          xianshi: "性格缺点  1、表面冷漠佔有欲很强，个性上有柔弱的一面不易亲近也不轻易表露真心，更不隨便与人交往。 "
        })
        break;
      case '7':
        this.setData({
          shuoming: "性格优点  1、性情开朗浪漫热情，善于词令，且有爽朗乐天的人生观。   "
        })

        _this.setData({
          xianshi: "性格缺点  1、血气刚强有忍耐力，脾气暴燥，沈迷于酒赌中。  "
        })
        break;
      case '8':
        this.setData({
          shuoming: "性格优点  1、凡事考虑周到，对四周事务处理妥当，有进取心， 善于交际，个性温柔具捨已成仁胸怀。   "
        })

        _this.setData({
          xianshi: "性格缺点  1、有时悲观犹豫不决，喜欢听天由命，不喜欢例行工作。"
        })
        break;
      case '9':
        this.setData({
          shuoming: "性格优点  1、才智高且具优秀的头脑，行动活泼好动且伶俐。   "
        })

        this.setData({
          xianshi: "性格缺点 1、平常爱说大话，有时有反对人之意见虛语或伪诈行为。忽略必需遵守社会全体规范，有点不 脚踏实地。  "
        })
        break;
      case '10':
        this.setData({
          shuoming: "性格优点  1、做事很稳定，有现代新潮派的大志向，脑筋转动很快。 "
        })

        _this.setData({
          xianshi: "性格缺点  1、具有忽冷忽热的心理，处事往往纸上谈兵很少付诸行动。  "
        })
        break;
      case '11':
        this.setData({
          shuoming: "性格优点  1、富于正义感讲义气，重人情道义，做事全力以赴。   "
        })

        _this.setData({
          xianshi: "性格缺点 1、感情起伏大，易燥易怒。 "
        })
        break;
      case '12':
        this.setData({
          shuoming: "性格优点  1、真诚正直，凡事认真实行，人缘极佳。  "
        })

        _this.setData({
          xianshi: "性格缺点  1、好睡重眠心地善良，对人没有猜疑而常受骗上当。"
        })
        break;

      default:
        break;
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})