// pages/xingzuo/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: [{
        key: 1,
        message: '白羊座 3月21日-4月19日',
      },
      {
        key: 2,
        message: '金牛座4月20日-5月20日'
      },
      {
        key: 3,
        message: '双子座5月21日-6月21日'
      },
      {
        key: 4,
        message: '巨蟹座6月22日-7月22日'
      },
      {
        key: 5,
        message: '狮子座7月23日-8月22日'
      },
      {
        key: 6,
        message: '处女座8月23日-9月22日'
      },
      {
        key: 7,
        message: '天秤座9月23日-10月23日'
      },
      {
        key: 8,
        message: '天蝎座10月24日-11月22日'
      },
      {
        key: 9,
        message: '射手座11月23日-12月21日'
      },
      {
        key: 10,
        message: '摩羯座12月22日-1月19日'
      },
      {
        key: 11,
        message: '水瓶座1月20日-2月18日'
      },
      {
        key: 12,
        message: '双鱼座2月19日-3月20日'
      },
    ]
  },
  go: function (e) {
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../xingzuo1/index?id=' + id
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (typeof this.getTabBar == "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 1
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})