// pages/xingzuo/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: [{
        key: 1,
        message: '鼠',
      },
      {
        key: 2,
        message: '牛'
      },
      {
        key: 3,
        message: '虎'
      },
      {
        key: 4,
        message: '兔'
      },
      {
        key: 5,
        message: '龙'
      },
      {
        key: 6,
        message: '蛇'
      },
      {
        key: 7,
        message: '马'
      },
      {
        key: 8,
        message: '羊'
      },
      {
        key: 9,
        message: '猴'
      },
      {
        key: 10,
        message: '鸡'
      },
      {
        key: 11,
        message: '狗'
      },
      {
        key: 12,
        message: '猪'
      },
    ]
  },
  go: function (e) {
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../shuxiang1/index?id=' + id
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (typeof this.getTabBar == "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 4
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})